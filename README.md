# Et ton maire il en pense quoi ?!
## La plateforme qui interpelle les maires sur la réforme de retraite 2023.

Front du site [et-ton-maire.fr](https://et-ton-maire.fr)

## Getting started

```sh
nvm use
yarn install
yarn dev
```

Ce dépôt ne contient pas les données que et-ton-maire.fr présente mais seulement le code de l'interface.

Pour toutes demandes : bonjour@et-ton-maire.fr
