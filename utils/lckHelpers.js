import feathers from '@feathersjs/feathers';
import rest from '@feathersjs/rest-client';
import auth from '@feathersjs/authentication-client';
import { COLUMN_TYPE } from '@locokit/lck-glossary';

const getOriginalColumn = column => {
  if (
    column.column_type_id !== COLUMN_TYPE.LOOKED_UP_COLUMN
    || (column.parents && column.parents.length === 0)
    || !column.parents
  ) {
    return column;
  }
  return getOriginalColumn(column.parents[0]);
};

const getColumnValue = (column, data) => {
  if (data === '' || data === undefined || data === null) {
    return data;
  }

  try {
    switch (column.column_type_id) {
      case COLUMN_TYPE.USER:
      case COLUMN_TYPE.GROUP:
      case COLUMN_TYPE.RELATION_BETWEEN_TABLES:
        return data.value;

      case COLUMN_TYPE.LOOKED_UP_COLUMN: {
        const originalColumn = getOriginalColumn(column);

        if ([
          COLUMN_TYPE.DATE,
          COLUMN_TYPE.SINGLE_SELECT,
          COLUMN_TYPE.MULTI_SELECT,
        ].includes(originalColumn.column_type_id)) {
          return getColumnValue(originalColumn, data.value);
        } if (originalColumn.column_type_id === COLUMN_TYPE.MULTI_USER) {
          return getColumnValue(originalColumn, data);
        }

        return data.value;
      }

      case COLUMN_TYPE.MULTI_USER:
        return data.value.join(', ');

      case COLUMN_TYPE.SINGLE_SELECT:
        return column.settings.values?.[data]?.label;

      case COLUMN_TYPE.MULTI_SELECT: {
        if (data.length > 0) {
          return data.map(d => column.settings.values?.[d]?.label).join(', ');
        }

        return '';
      }

      case COLUMN_TYPE.FORMULA:
      case COLUMN_TYPE.DATE:
      default:
        return data;
    }
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error('Field with bad format', data, error);

    return '';
  }
};

export const transposeByLabel = (table, tableSchema) => {
  const transposedTable = table.map(row =>
    tableSchema.columns.reduce(
      (acc, currentColumn) => ({
        ...acc,
        [currentColumn.text]: getColumnValue(currentColumn, row.data[currentColumn.id]),
      }),
      { id: row.id },
    ));

  return transposedTable;
};

export const lckApi = async (dbUuid) => {
  const lckClient = feathers();

  // Connect to a different URL
  const restClient = rest(`https://locokit.makina-corpus.net/api`);

  // Configure an AJAX library (see below) with that client
  lckClient.configure(restClient.fetch(window.fetch.bind(window)));
  lckClient.configure(auth());

  const authenticate = () => lckClient.authenticate({
    strategy: 'local',
    email: 'et-mon-maire-lck-readonly@yopmail.com',
    password: 'et-mon-maire-LCK-readonly-2023',
  });

  const { user } = await authenticate();

  const { groups: [{ id: groupId }] } = await lckClient.service('user').get(user.id, {
    query: {
      $eager: 'groups',
    },
  });

  const schema = await lckClient.service('database')
  .get(dbUuid, {
    query: {
      $eager: '[tables.[columns,views.[columns]]]',
    },
  });

  const getRows = (id, query = {}) => lckClient.service('row').find({
    query: {
      table_id: id,
      $lckGroupId: groupId,
      ...query,
    },
  });

  const query = async (tableUuid, query = {}) => {
    const tableSchema = schema.tables.find(({ id }) => id === tableUuid);
    const tableRows = await getRows(tableSchema.id, query);
    const readableRows = transposeByLabel(tableRows.data, tableSchema);

    return readableRows;
  }

  const count = async (tableUuid, query = {}) => {
    const tableSchema = schema.tables.find(({ id }) => id === tableUuid);
    const tableRows = await getRows(tableSchema.id, {'$limit': 0, ...query});

    return tableRows.total;
  }

  return {
    LCK: lckClient,
    query,
    count,
  };
};

