// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: [
    '~/assets/css/main.scss'
  ],
  modules: [
    '@nuxtjs/tailwindcss',
  ],
  app: {
    head: {
      title: 'Et ton maire ?!',
      meta: [
        { charset: 'utf-8' },
        {
          hid: 'description',
          name: 'description',
          content: `Et ton maire, il en pense quoi ? La plateforme citoyenne qui implique les maires dans la politique nationale`,
        },
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
        { name: 'format-detection', content: 'telephone=no' },
      ],
      link: [
        {rel: 'icon', type: 'image/x-icon', href: '/favicon.svg' }
      ],
      viewport: 'width=500, initial-scale=1',
      script: [
        {
          src: 'https://gc.zgo.at/count.js',
          async: true ,
          'data-goatcounter': 'https://et-ton-maire.goatcounter.com/count',
        }
      ],
    },
  },
  components: true,
})
